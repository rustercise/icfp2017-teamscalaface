package lambda
package traceur
import scalax.collection.Graph
import scalax.collection.GraphEdge.UnDiEdge
import scalax.collection.GraphPredef._
import scala.collection.Map
import scala.collection.mutable.HashMap
import scala.math.pow
import scala.util.Random

import io.circe._,  io.circe.generic.auto._, io.circe.parser._, io.circe.syntax._

object Types {
	type PunterId = Int
	type SiteId = Int
	type SiteGraph = Graph[Site, UnDiEdge]
	type PathType = SiteGraph#Path

	abstract sealed case class River(source: SiteId, target: SiteId) {
		def edge(): UnDiEdge[Site] = { Site(source) ~ Site(target) }
	}

	object River {
		/* fun hack to ensure River.source < River.target */
		def apply(source: SiteId, target: SiteId): River = {
			if (source == target) {
				throw new IllegalArgumentException("no circular rivers thx")
			} else if (source < target) {
				new River(source, target){}
			} else {
				new River(target, source){}
			}
		}

		def apply(source: Site, target: Site): River = apply(source.id, target.id)

		implicit def edge2river(edge: SiteGraph#EdgeT) = River(edge._1.value, edge._2.value)
	}
	sealed abstract class Move {
		val punter: PunterId
		def claims(): List[(PunterId, River)]
	}
	case class Claim(punter: PunterId, river: River) extends Move {
		override def claims() = (punter, river) :: Nil
	}
	case class Pass(punter: PunterId) extends Move {
		override def claims() = Nil
	}
	case class Splurge(punter: PunterId, route: List[SiteId]) extends Move {
		override def claims() = route.iterator.sliding(2).map(l ⇒ (punter, River(l(0), l(1)))).toList
	}
	case class ClaimOption(punter: PunterId, river: River) extends Move {
		override def claims() = (punter, river) :: Nil
	}

	implicit val encodeMove: Encoder[Move] = new Encoder[Move] {
		final def apply(m: Move): Json = {
			m match {
			case c @ Claim(_, _) => Json.obj(("claim", c.asJson))
			case p @ Pass(_) => Json.obj(("pass", p.asJson))
			case s @ Splurge(_, _) => Json.obj(("splurge", s.asJson))
			case o @ ClaimOption(_, _) => Json.obj(("option", o.asJson))
			}
		}
	}

	implicit val decodeMove: Decoder[Move] = new Decoder[Move] {
		final def apply(c: HCursor): Decoder.Result[Move] = {
			c.fields match {
			case Some(Vector("claim")) => c.get[Claim]("claim")
			case Some(Vector("pass")) => c.get[Pass]("pass")
			case Some(Vector("splurge")) => c.get[Splurge]("splurge")
			case Some(Vector("option")) => c.get[ClaimOption]("option")
			case _ => Left(DecodingFailure("not a valid move: " + c.value.noSpaces, c.history))
			}
		}
	}

	implicit val encodeClaim: Encoder[Claim] = new Encoder[Claim] {
		final def apply(c: Claim): Json = Json.obj(("punter", c.punter.asJson), ("source", c.river.source.asJson), ("target", c.river.target.asJson))
	}

	implicit val encodeClaimOption: Encoder[ClaimOption] = new Encoder[ClaimOption] {
		final def apply(c: ClaimOption): Json = Json.obj(("punter", c.punter.asJson), ("source", c.river.source.asJson), ("target", c.river.target.asJson))
	}

	private def decodeClaimTuple(c: HCursor): Decoder.Result[(PunterId, River)] = {
		(c.get[PunterId]("punter"), c.get[SiteId]("source"), c.get[SiteId]("target")) match {
		case (Right(p), Right(s), Right(t)) => Right((p, River(s, t)))
		case _ => Left(DecodingFailure("not a valid claim: " + c.value.noSpaces, c.history))
		}
	}

	implicit val decodeClaim: Decoder[Claim] = new Decoder[Claim] {
		final def apply(c: HCursor): Decoder.Result[Claim] = decodeClaimTuple(c).map({case (p,r) => Claim(p,r)})
	}

	implicit val decodeClaimOption: Decoder[ClaimOption] = new Decoder[ClaimOption] {
		final def apply(c: HCursor): Decoder.Result[ClaimOption] = decodeClaimTuple(c).map({case (p,r) => ClaimOption(p,r)})
	}


	type Score = (PunterId, Int)

	case class ServerFeatures(futures: Boolean = false, splurges: Boolean = false, options: Boolean = false)
	val NoServerFeatures = ServerFeatures()

	case class RawMap(sites: List[Site], rivers: List[River], mines: List[Site])

	implicit val decodeSettings: Decoder[ServerFeatures] = new Decoder[ServerFeatures] {
		final def apply(c: HCursor): Decoder.Result[ServerFeatures] = {
			Right(ServerFeatures(
				c.get[Boolean]("futures").getOrElse(false),
				c.get[Boolean]("splurges").getOrElse(false),
				c.get[Boolean]("options").getOrElse(false)
			))
		}
	}

    implicit val encodeGraph: Encoder[SiteGraph] = new Encoder[SiteGraph] {
         final def apply(a: SiteGraph): Json = (Json.obj( ("nodes", a.nodes.map((x: SiteGraph#NodeT) => x.value).asJson), ("edges", (for {
            edges <- a.edges
         } yield {
            (edges._1.value.id, edges._2.value.id)
         }).asJson) ))
     }

     implicit val decodeGraph: Decoder[SiteGraph] = new Decoder[SiteGraph] {
         final def apply(c: HCursor): Decoder.Result[SiteGraph] = Right(Graph.from(
            (c.downField("nodes").focus.head.asArray.get.map((x => x.as[Site].right.get))),
            (c.downField("edges").focus.head.asArray.get.map((x => Site(x.asArray.get(0).as[SiteId].right.get) ~ Site(x.asArray.get(1).as[SiteId].right.get))))
         ))
     }

	class Site private(val id: SiteId, val xy: Option[(Float,Float)] = None) {
		private val distanceCache : HashMap[SiteId, Int] = HashMap.empty[SiteId, Int]

		/* returns Some(distance) to another site, if known (otherwise returns None) */
		def distanceTo(other: SiteId) : Option[Int] = distanceCache.synchronized {
			distanceCache.get(other)
		}

		/* returns distance to another site, calculating it via distFn if necessary */
		def distanceTo(other: SiteId, distFn: () ⇒ Int) : Int = distanceCache.synchronized {
			distanceCache.get(other) match {
				case Some(d) => d
				case None => {
					val d = distFn()
					distanceCache.put(other, d)
					d
				}
			}
		}

		override def toString() : String = {
			id.toString
		}

		override def hashCode() : Int = {
			id.hashCode
		}
	}

	object Site {
		private val instances = new java.util.HashMap[SiteId, Site]

		def apply(id: SiteId, xy: Option[(Float,Float)] = None) : Site = instances.synchronized {
			var s = instances.get(id)
			if (s == null) {
				s = new Site(id)
				instances.put(id, s)
			}
			return s
		}

		implicit def site2id(site: Site) : SiteId = { site.id }

		/* a Site is encoded as a raw number if its distanceCache is empty, otherwise {id:X, d:[[id2,dist2],...]} */
		implicit val encodeSite: Encoder[Site] = new Encoder[Site] {
			final def apply(site: Site): Json = {
				if (site.distanceCache.isEmpty)
					site.id.asJson
				else
					Json.obj(("id", site.id.asJson), ("d", site.distanceCache.toList.asJson))
			}
		}

		implicit val decodeSite: Decoder[Site] = new Decoder[Site] {
			final def apply(c: HCursor): Decoder.Result[Site] = {
				c.focus match {
					case Some(json) if json.isNumber => Right(Site(c.as[SiteId].right.get))
					case Some(json) if json.isObject => {
						val obj = json.asObject.get
						val s = Site(obj("id").get.as[SiteId].right.get)
						obj("d").getOrElse(Json.Null).as[Vector[(SiteId, Int)]] match {
							case Right(vec) =>
								for ((target, distance) <- vec) {
									s.distanceTo(target, () => {distance}: Int)
								}
							case Left(_) =>
						}
						Right(s)
					}
					case None => Left(DecodingFailure("invalid Site", c.history))
				}
			}
		}
	}
}

