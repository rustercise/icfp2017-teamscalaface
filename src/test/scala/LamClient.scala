import scalax.collection.Graph
import scalax.collection.GraphPredef._
import scalax.collection.io.dot._
import scala.math.pow

import org.scalatest.{ FlatSpec, Matchers }
import lambda.traceur.onlinemsg.Msg
import lambda.traceur.onlinemsg.Msg._
import lambda.traceur.Types._
import lambda.traceur._
import lambda.traceur.lamclient._
import lambda.traceur.helpers.Helpers._
import java.io.PrintWriter
import sys.process._
import java.net.{ServerSocket, Socket}
import java.io._
import resource._
import scala.util.control.Breaks._
import io.circe._, io.circe.generic.auto._, io.circe.parser._, io.circe.syntax._

class LamClientSpec extends FlatSpec with Matchers {
  var sample = scala.io.Source.fromFile("samples/sample1.json").mkString
  it should "parse messagess" in {
    var msgs = List(
        """{"you":"blinken"}""",
        """{"punter":1,"punters":2,"map":"""+sample+"}",
        """{"timeout": 0.75}""",
        """{"claim" : {"punter" : 2, "source" : 0, "target" : 1}}""")
    var stream = msgs.map(LamClient.buildPacket).mkString
    val write = new PrintWriter(new BufferedWriter(new StringWriter()))
    val read = new BufferedInputStream(new ByteArrayInputStream(stream.getBytes))

    LamClient.runGame(write, read, new MagicBrain())
  }

  it should "cope with various settings" in {
    def trySettings(settings: Option[String]) = {
      val setup = """{"punter":1,"punters":2,"map":"""+sample + (settings match {case Some(s) => (", " + s) case None => ""}) + "}"
      var msgs = List(
          """{"you":"blinken"}""",
          setup,
          """{"move":{"moves":[{"claim":{"punter": 2, "source" : 0, "target" : 1}}]}}""",
          """{"stop":{"moves":[], "scores":[]}}"""
      )
      var stream = msgs.map(LamClient.buildPacket).mkString
      val write = new PrintWriter(new BufferedWriter(new StringWriter()))
      val read = new BufferedInputStream(new ByteArrayInputStream(stream.getBytes))
      val brain = new MagicBrain()
      LamClient.runGame(write, read, brain)
    }
    trySettings(Some(""""settings":{"futures": true}"""))
    trySettings(None)
    trySettings(Some(""""settings":{"futures": true, "splurges": true}"""))
    trySettings(Some(""""settings":{"futures": true, "splurges": true, "options": true, "unsupported": true}"""))
  }
}


