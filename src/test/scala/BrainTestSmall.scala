import org.scalatest.{FlatSpec,Matchers}

import lambda.traceur._
import lambda.traceur.BrainHelp._
import lambda.traceur.Types._
import lambda.traceur.helpers.Helpers._

import io.circe._, io.circe.generic.auto._, io.circe.parser._, io.circe.syntax._

class BrainSmallSpec extends FlatSpec with Matchers {
	val me : PunterId = 1
	val sample = scala.io.Source.fromFile("samples/nara-sparse.json").mkString

	it should "generate mine ordering shortest" in {
		var brain = new MagicBrain()
		var state = brain.init(me, 2, decode[RawMap](sample).right.get)
		val res = brain.getMinesShortest(state.mines,state.graph)
		println(res)
		res should be (idsToSites(List(760, 1552, 0, 1510, 243, 455, 561, 262, 176, 1105, 1019, 754)))
	}


	it should "generate mine ordering fastest" in {
		var brain = new MagicBrain()
		var state = brain.init(me, 2, decode[RawMap](sample).right.get)
		val res = brain.getMinesFastest(state.mines,state.graph)
		println(res)
		res should be (idsToSites(List(760, 1552, 243, 455, 262, 176, 1105, 0, 1510, 561, 1019, 754)))
	}


	it should "get strategy should return futures" in {
		var brain = new MagicBrain()
		var state = brain.init(me, 2, decode[RawMap](sample).right.get)
		println("futures: "+state.futures)
		println("targetSites: "+state.targetSites)
		// res should be (idsToSites(List(760, 1552, 0, 1510, 243, 455, 561, 262, 176, 1105, 1019, 754)))
	}

}



class BrainStateSpec extends FlatSpec with Matchers {
	val me : PunterId = 1
	val map = loadMap("samples/circle.json")

	it should "correctly serialise state" in {
		var brain = new MagicBrain()
		var state = brain.init(me, 2, map)
		Site(18).distanceTo(Site(12), () => {2})
		var json = state.asJson
		//println(json.noSpaces)
		json.as[ClaimedEdges] match {
			case Left(err) => {println(err); fail(err.toString)}
			case Right(_) =>
		}
	}
}
